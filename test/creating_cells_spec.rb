require 'support/test_helper'

describe "Creating Cells" do
  before do
    cellular_data = [[States::Dead, States::Alive, States::Dead], [States::Dead, States::Alive, States::Dead]]
    soup_factory = SoupFactory.new(cellular_data)
    @soup = soup_factory.create_soup_with_cells
  end

  it "must have cells that have position and state methods" do
    cell_1_0 = @soup.cells[3]
    cell_1_0.dead?.must_equal true
    cell_1_0.pos_x.must_equal 1
  end

  it "must have cells with properties in correct sequence" do
    cells_data = @soup.cells.map do |cell|
      [cell.alive?, cell.pos_x, cell.pos_y]
    end

    cells_data.must_equal [[false, 0, 0], [true, 0, 1], [false, 0, 2], [false, 1, 0], [true, 1, 1], [false, 1, 2]]
  end
end
