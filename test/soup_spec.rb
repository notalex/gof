require 'support/test_helper'

describe "Soup" do
  it "must create a list of cell states" do
    cellular_data = [[States::Dead, States::Alive, States::Dead], [States::Dead, States::Alive, States::Dead], [States::Dead, States::Alive, States::Dead]]
    soup = SoupFactory.new(cellular_data).create_soup_with_cells
    soup.positioned_cell_states.must_equal cellular_data
  end
end
