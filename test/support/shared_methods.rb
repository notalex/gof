def blinker_pattern
  <<-END.gsub(/^\s+/, '')
    - x -
    - x -
    - x -
  END
end
