require 'support/test_helper'

describe "Translator" do
  it "must translate input file into an array" do
    Helpers::ArrayConverter.new(blinker_pattern).get[0].must_equal %w(- x -)
  end

  it "must translate an array into matching states" do
    array = [%w(- x -), %w(- x -), %w(- x -)]
    Helpers::CellStateInterpreter.new(array).translate[0].must_equal [States::Dead, States::Alive, States::Dead]
  end

  it "must decode a list of cells" do
    cellular_data = [[States::Dead, States::Alive, States::Dead], [States::Dead, States::Alive, States::Dead], [States::Dead, States::Alive, States::Dead]]
    soup = SoupFactory.new(cellular_data).create_soup_with_cells
    Helpers::CellStateInterpreter.decode(soup.positioned_cell_states).must_equal [%w(- x -), %w(- x -), %w(- x -)]
  end
end
