require 'support/test_helper'

describe "Cell" do
  before do
    cellular_data = [[States::Dead, States::Alive, States::Dead], [States::Dead, States::Alive, States::Dead], [States::Dead, States::Alive, States::Dead]]
    soup_factory = SoupFactory.new(cellular_data)
    @soup = soup_factory.create_soup_with_cells
  end

  it "wont include self as a neighbour" do
    cell = @soup.cells[4]
    cell.neighbours.wont_include cell
  end

  it "cell_0_0_must have 3 neighbours" do
    cell = @soup.find_cell_by_position(0, 0)
    cell.neighbours.size.must_equal 3
  end

  it "cell_0_1_must have 1 alive neighbour" do
    cell = @soup.find_cell_by_position(0, 1)
    cell.alive_neighbour_count.must_equal 1
  end
end
