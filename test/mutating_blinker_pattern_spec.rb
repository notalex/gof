require 'support/test_helper'

describe "Mutating blinker pattern" do
  before do
    cellular_data = [[States::Dead, States::Alive, States::Dead], [States::Dead, States::Alive, States::Dead], [States::Dead, States::Alive, States::Dead]]
    soup = SoupFactory.new(cellular_data).create_soup_with_cells
    @soup = Brew.new(soup).mutate
  end

  it "must kill top row cells " do
    cell_0_1 = @soup.find_cell_by_position(0, 1)
    cell_0_1.dead?.must_equal true
  end

  it "must have living middle row cells" do
    cell_1_1 = @soup.find_cell_by_position(1, 1)
    cell_1_1.alive?.must_equal true
  end

  it "must pass the blinker pattern" do
    states = @soup.cells.map(&:alive?)
    states.must_equal [false, false, false, true, true, true, false, false, false]
  end
end
