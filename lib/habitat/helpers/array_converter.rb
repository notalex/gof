module Habitat
  module Helpers
    class ArrayConverter
      attr_reader :data

      def initialize(data)
        @data = data
      end

      def get
        rows = data.split("\n")
        rows.map { |string| string.split }
      end
    end
  end
end
