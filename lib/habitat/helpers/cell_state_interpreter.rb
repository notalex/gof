module Habitat
  module Helpers
    class CellStateInterpreter
      attr_reader :data

      STATE_DICTIONARY = {
        'x' => States::Alive,
        '-' => States::Dead
      }

      def initialize(data)
        @data = data
      end

      def translate
        data.map do |array|
          array.map { |state| STATE_DICTIONARY[state] }
        end
      end

      class << self
        def decode(states)
          states.map do |array|
            array.map { |state| inverted_state_dictionary[state] }
          end
        end

        private ###

        def inverted_state_dictionary
          STATE_DICTIONARY.invert
        end
      end
    end
  end
end
