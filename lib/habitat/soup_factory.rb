module Habitat
  class SoupFactory
    attr_reader :cellular_grid, :soup

    def initialize(cellular_grid)
      @cellular_grid = cellular_grid
      @soup = Soup.new
    end

    def create_soup_with_cells
      add_cells
      soup
    end

    private ###

    def add_cells
      cellular_grid.each_with_index do |cellular_row, i|
        cellular_row.each_with_index do |state, j|
          soup.add_cell Cell.new(state, soup, i, j)
        end
      end
    end
  end
end
