module Habitat
  class Cell
    attr_reader :pos_x, :pos_y, :soup, :state

    def initialize(state, soup, pos_x, pos_y)
      extend(state)

      @state = state
      @soup = soup
      @pos_x = pos_x
      @pos_y = pos_y
    end

    def alive_neighbour_count
      neighbours.select(&:alive?).count
    end

    def neighbours
      other_cells.select do |other_cell|
        (pos_x.pred..pos_x.succ) === other_cell.pos_x &&
        (pos_y.pred..pos_y.succ) === other_cell.pos_y
      end
    end

    private ###

    def other_cells
      soup.cells - [self]
    end
  end
end
