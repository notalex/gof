module Habitat
  class Brew
    attr_reader :soup, :mutated_soup

    def initialize(soup)
      @soup = soup
      @mutated_soup = Soup.new
    end

    def mutate
      soup.cells.each do |cell|
        count = cell.alive_neighbour_count

        if count == 3 || count == 2 && cell.alive?
          mutated_soup.add_cell new_mutated_cell(States::Alive, cell)
        else
          mutated_soup.add_cell new_mutated_cell(States::Dead, cell)
        end
      end

      mutated_soup
    end

    private ###

    def new_mutated_cell(state, cell)
      Cell.new(state, mutated_soup, cell.pos_x, cell.pos_y)
    end
  end
end
