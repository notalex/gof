module Habitat
  class Soup
    def initialize
      @cells = []
    end

    def cells
      @cells.dup
    end

    def find_cell_by_position(pos_x, pos_y)
      cells.find { |cell| [pos_x, pos_y] == [cell.pos_x, cell.pos_y] }
    end

    def add_cell(cell)
      @cells << cell
    end

    def positioned_cell_states
      grid_positioned_cells.map do |grouped_cells|
        grouped_cells.map do |cell|
          cell.state
        end
      end
    end

    private ###

    def grid_positioned_cells
      cells.chunk { |cell| cell.pos_x }.map { |grouping_factor, grouped_cells| grouped_cells }
    end
  end
end
